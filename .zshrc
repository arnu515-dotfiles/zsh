source $ZDOTDIR/scripts/ensure-zap-installed.zsh

# Load zap-zsh
source $ZAP_DIR/zap.zsh

# Load plugins
source $ZDOTDIR/zsh-plugins.zsh

# Load zoxide
eval "$(zoxide init zsh --cmd cd)"

# Load starship prompt
export STARSHIP_CONFIG="$ZDOTDIR/starship/starship.toml"
eval "$(starship init zsh)"

# Load nix
if [ -f ~/.nix-profile/etc/profile.d/nix-daemon.sh ]; then
	source ~/.nix-profile/etc/profile.d/nix-daemon.sh
fi

# Load direnv
eval "$(direnv hook zsh)"

# Load my aliases
source $ZDOTDIR/aliases.zsh

# Load local.zsh. This file is great for storing sensitive information.
# It is not committed to git.
if [ -f $ZDOTDIR/local.zsh ]; then
    source $ZDOTDIR/local.zsh
fi

# Load completions
autoload -Uz compinit && compinit

