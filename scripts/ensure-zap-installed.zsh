export ZAP_DIR="${XDG_DATA_DIR:-$HOME/.local/share}/zap"

if [ ! -d $ZAP_DIR ]; then
    echo "Installing zap-zsh plugin manager..."
    mkdir -p $ZAP_DIR
    git clone -b 'master' https://github.com/zap-zsh/zap.git "$ZAP_DIR" &> /dev/null
    echo "Installed zap-zsh plugin manager"
fi
