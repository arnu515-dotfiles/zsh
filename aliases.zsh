if command -v bat > /dev/null 2>&1; then
  alias cat="bat"
fi

alias l="ls -al"
alias ll="ls -l"
alias la="ls -a"

if command -v exa > /dev/null 2>&1; then
  alias eza="exa"
fi

if command -v eza > /dev/null 2>&1; then
  alias ls="eza --group-directories-first --icons=always"
fi

if command -v zellij > /dev/null 2>&1; then
  alias z="zellij"
  alias za="zellij attach --create"
fi

if command -v helix > /dev/null 2>&1; then
  alias h="helix"
fi
if command -v hx > /dev/null 2>&1; then
  alias h="hx"
fi

alias restart_plasma="pkill -ARBT plasmashell"

if command -v fastfetch > /dev/null 2>&1; then
	alias neofetch="fastfetch"
fi

if command -v dnf5 > /dev/null 2>&1; then
  # Access dnf4 using /usr/bin/dnf4
  alias dnf="dnf5"
fi
