# ZSH config

Uses the [`zap`](https://github.com/zap-zsh/zap) plugin manager, and also the [`starship`](https://starship.rs) prompt.

## Use this config

1. Clone this repository to your `$XDG_CONFIG_HOME`.

```
git clone https://github.com/arnu515-dotfiles/zsh "${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
```

2. Restart your `zsh`. If it still doesn't work, add a `~/.zprofile` with the following content:

```
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
```
