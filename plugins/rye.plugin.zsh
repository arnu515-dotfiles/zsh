#!/usr/bin/env zsh
# Return if the directory '$RYEHOME' or '$HOME/.rye' does not exist
if [[ ! -d "${RYEHOME:-$HOME/.rye}" ]]; then
	return
fi

source "${RYEHOME:-$HOME/.rye}/env"
eval "$(rye self completion -s zsh)"
