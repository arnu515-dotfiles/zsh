#!/usr/bin/env zsh
# Exit if the 'mise' command can not be found
if ! (( $+commands[mise] )); then
    return
fi

# Activate `mise` within the zsh shell and provide access to any
# globally insstalled tools within your `.zshrc` file.
# https://mise.jdx.dev/dev-tools/shims.html#zshrc-bashrc-files
eval "$(mise activate zsh)"
eval "$(mise hook-env -s zsh)"
eval "$(mise completions zsh)"
